package br.cepel.config;

import static org.eclipse.persistence.config.PersistenceUnitProperties.DDL_GENERATION;
import static org.eclipse.persistence.config.PersistenceUnitProperties.DROP_AND_CREATE;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_DRIVER;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_PASSWORD;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_URL;
import static org.eclipse.persistence.config.PersistenceUnitProperties.JDBC_USER;
import static org.eclipse.persistence.config.PersistenceUnitProperties.LOGGING_LEVEL;

import java.util.HashMap;
import java.util.Map;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;

import org.junit.BeforeClass;
import org.junit.Test;

import br.cepel.asset.PersistenceUnitName;
import br.cepel.common.Configuration;
import common.PersistenceManager;
import model.entity.messor.Acquisitor;
import model.entity.messor.Channel;
import model.entity.messor.Messor;

public class SaveSomaConfigTestForMatrix {

	private static String persistenceUnit = null;

	private static final String somaConfigFilename = "src/main/resources/client/LabAt4/"
			+ "SomaConfigTeste-AT-4-foz.xml";

	private static SomaConfigPersistenceHelper dbHelper = new SomaConfigPersistenceHelper();

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Configuration.initializeForJUnitTest(".");
		final String ORACLE_URL = "jdbc:oracle:thin:@//localhost:1521/soma";
		final String H2_URL = "jdbc:h2:../DATABASE/test_soma";

		Map<String, String> additionalParams = new HashMap<String, String>();
		// WARNING � um nivel bom para produ��o
		additionalParams.put(LOGGING_LEVEL, "FINEST");

		System.out.println("URL poss�veis : " + H2_URL + " ou " + ORACLE_URL);
		setDBMSProperties(additionalParams, H2_URL);

		// DDL_GENERATION : CREATE_ONLY ou DROP_AND_CREATE
		additionalParams.put(DDL_GENERATION, DROP_AND_CREATE);

		persistenceUnit = PersistenceUnitName.SOMA_PERSISTENCE.getName();
		PersistenceManager.startPersistenceUnit(PersistenceUnitName.SOMA_PERSISTENCE, additionalParams);

		dbHelper.setupPersistence(persistenceUnit);
	}

	private static void setDBMSProperties(Map<String, String> additionalParams, String url) {
		final String ORACLE_DRIVER = "oracle.jdbc.OracleDriver";
		// "jdbc:mysql://localhost:3306/soma"
		final String USER = "soma";
		final String PASSWD = "soma_123";
		if (url.startsWith("H2")) {
			additionalParams.put(JDBC_DRIVER, "org.h2.Driver");
			additionalParams.put(JDBC_URL, url);
		} else {
			additionalParams.put(JDBC_DRIVER, ORACLE_DRIVER);
			additionalParams.put(JDBC_URL, url);
			additionalParams.put(JDBC_USER, USER);
			additionalParams.put(JDBC_PASSWORD, PASSWD);
		}
	}

	@Test
	public void saveConfigOnDB() throws Exception {

		JAXBContext jc = JAXBContext.newInstance(SomaConfig.class, Messor.class, Acquisitor.class, Channel.class);

		Unmarshaller unmarshaller = jc.createUnmarshaller();
		StreamSource xml = new StreamSource(somaConfigFilename);
		SomaConfig somaConfig = (SomaConfig) unmarshaller.unmarshal(xml);

		dbHelper.saveConfigOnDB(somaConfig);
	}
}
