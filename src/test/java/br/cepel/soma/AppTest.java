package br.cepel.soma;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

import br.cepel.config.SaveSomaConfigTestForMatrix;

@RunWith(Suite.class)
@Suite.SuiteClasses({ SaveSomaConfigTestForMatrix.class
})
public class AppTest {
	// normally, this is an empty class
}
